var _isUndefined = require('lodash/isUndefined');

var Response = {
  setHeaders: function (res) {
    res.set('Access-Control-Allow-Origin', '*');
    res.set('Access-Control-Allow-Credentials', 'true');
  },
  success: function (res, data) {
    Response.setHeaders(res);
    res.status(200);
    res.json(data);
  },
  error: function (res, statusCode, error) {
    Response.setHeaders(res);
    error = _isUndefined(error) ? error : {};
    res.status(statusCode);
    res.json(error);
  }
};

module.exports = Response;