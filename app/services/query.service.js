var YQL = require('yql');

var _isArray = require('lodash/isArray');

var Query = {
  getInfoQuery: function (zip) {
    var query;
    if (_isArray(zip)) {
      query = new YQL('select * from weather.forecast where woeid in (select woeid from geo.places(1) where text IN (' + zip.toString() + '))');
    } else {
      query = new YQL('select * from weather.forecast where woeid in (select woeid from geo.places(1) where text="' + zip + '")');
    }
    return query;
  }
};

module.exports = Query;