var Query = require('../services/query.service.js');
var Response = require('../services/response.service.js');

var _has = require('lodash/has');
var _hasKeysNested = require('../services/has-keys-nested.js');

var WeatherController = {
  getInfo: function (req, res) {
    var zip = req.query.zip;
    var query = Query.getInfoQuery(zip);

    var execSuccess = function (err, data) {
      if (_has(data, 'error')) {
        Response.error(res, 400, data);
        return;
      }

      if (!_hasKeysNested(data, 'query', 'results', 'channel')) {
        Response.error(res, 400);
        return;
      }

      Response.success(res, data.query.results.channel);
    };

    query.exec(execSuccess);
  }
};

module.exports = WeatherController;