var express = require('express');
var app = express();

var WeatherController = require('./controllers/weather.controller');

app.get('/', WeatherController.getInfo);

app.listen(3000, function () {
    console.log('Listening on port 3000');
});